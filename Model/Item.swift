//
//  Item.swift
//  TodoApp
//
//  Created by LuisT on 9/5/18.
//  Copyright © 2018 LuisT. All rights reserved.
//

import Foundation

struct Item {
    let title:String;
    let location:String;
    let description:String;
    
    
}
