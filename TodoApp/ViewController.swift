//
//  ViewController.swift
//  TodoApp
//
//  Created by LuisT on 8/5/18.
//  Copyright © 2018 LuisT. All rights reserved.
//

import UIKit

/* Para utilizar tablas, se usa dos protocolos que  se requieren:
 - UITableViewDataSource: maneja los datos de la tabla. Se deben implementar los metodos que requiere este protocolo.
    a) numberOfSections
    b) tableView (numberOfRowsInSection)
    c) tableView (cellForRowAt)
 
 - UITableViewDelegate: para controlar la UI de la tabla y las interacciones del user con la tabla. No tiene metodos requeridos como el anterior protocolo.
 
 Adicional, desde el StoryBoard, debemos indicar en que archivo esta el Datasource y el Delegate. (control + clic y arrastro al viewController y senalamos datasource y delegate)

 */
class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let itemManager = ItemManager();
    
    // Referencia a la tabla para ejecutar funciones.
    @IBOutlet weak var itemTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
//        let item1 = Item(title: "ToDo1", location: "Office", description: "Turn on computer.");
//        let item2 = Item(title: "ToDo2", location: "House", description: "Laundry.");
//        let item3 = Item(title: "ToDo3", location: "House", description: "Study?.");
//
//        itemManager.toDoItems = [item1, item2, item3];
//
//        let item4 = Item(title: "ToDo4", location: "House", description: "Level Up WoW character :D ");
//        itemManager.doneItems = [item4];
        
    }

    // Es una funcion para ver desde donde voy y hacia donde voy (Navegacion)
    // Quien soy, donde estoy, etc.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        /*NOTAS: Siempre dar identificadores a los segues.
         Solo hay un prepare por viewController, en donde se debe evaluar el segue que se ejecuta
         con su identificador y ejecutamos algo correspondiente.
         El prepare se ejecuta despues de cualquier metodo que inicie navegacion (ex: performSegue(withIdentifier: "toDoneItemSegue", sender: self))
         */
        if (segue.identifier == "toAddItemSegue") {
            // Determino el destino del segue.
            let destination = segue.destination as! AddItemViewController
            // Y en el destino, igualo la variable item manager a la itemManager del origen.
            destination.itemManager = itemManager;
        }
        
        if (segue.identifier == "toDoneItemSegue") {
            let destination = segue.destination as! DoneItemsViewController
            let selectedRow = itemTableView.indexPathForSelectedRow!
            destination.itemInfo = (itemManager, selectedRow.row)
        }
        
    }
    
    // Se ejecuta cada vez que la vista aparece.
    override func viewWillAppear(_ animated: Bool) {
        // le indicamos a la tabla que debe recargar la data
        itemTableView.reloadData()
    }
    
    //=============> TABLE IMPLEMENTATION
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2; // TODO section and DONE section
    }
    
    // ESTA FUNCION DEBE RETORNAR EL NUMERO DE ITEMS POR CADA SECTION DE LA TABLA.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0) {
            return itemManager.toDoItems.count;
        }
        return itemManager.doneItems.count;
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "item1")// PARA UTILIZAR EL DETAIL LABEL, SE DEBE USAR PROTOTYPE CELLS (ESPECIFICARLAS DESDE LAS PROPIEDADES DEL TABLEVIEW).
        if (indexPath.section == 0) {
            cell?.textLabel?.text = itemManager.toDoItems[indexPath.row].title;
            cell?.detailTextLabel?.text = itemManager.toDoItems[indexPath.row].description;
            
            if (indexPath.row % 2 == 0) {
                cell?.backgroundColor = UIColor.lightGray;
            } else {
                cell?.backgroundColor = UIColor.darkGray;
            }
        } else {
            cell?.textLabel?.text = itemManager.doneItems[indexPath.row].title;
            cell?.detailTextLabel?.text = itemManager.doneItems[indexPath.row].description;
            if (indexPath.row % 2 == 0) {
                cell?.backgroundColor = UIColor.cyan;
            } else {
                cell?.backgroundColor = UIColor.blue;
            }
        }
        return cell!; // VA AQUI CON SIGNO DE ADMIRACION PORQUE NECESITO QUE RETORNE ALGO SIEMPRE
    }
    
// -> TITULO DEL HEADER - SIMPLE HEADERS
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return section == 0 ? "To Do" : "Done"
//    }
    

// -> TITULO DEL HEADER -> CUSTOM HEADERS 1/2
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if (section == 0 ) {
            let sectionToDo = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
            let label  = UILabel()
            label.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width - 70, height: 40)
            label.font = UIFont.boldSystemFont(ofSize: 18)
            label.textColor = UIColor.white
            label.textAlignment = .left
            label.text = "To Do"
            sectionToDo.addSubview(label);
            sectionToDo.backgroundColor = UIColor.orange
            return sectionToDo;
        } else {
            let sectionDone = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
            let label  = UILabel()
            label.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width - 70, height: 40)
            label.font = UIFont.boldSystemFont(ofSize: 18)
            label.textColor = UIColor.black
            label.textAlignment = .left
            label.text = "Done"
            sectionDone.addSubview(label);
            sectionDone.backgroundColor = UIColor.green
            return sectionDone;
        }
    }

    // -> TITULO DEL HEADER -> CUSTOM HEADERS 2/2
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40;
    }
    
    // WILLSELECT: VALIDACIONES ANTES DE TOCAR LA CELDA
//    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
//
//    }
    
    // -> SELECCION DE ROWS DE LA TABLA
    // DIDSELECT: CUANDO YA TOCO LA CELDA
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // SOLO SELECCIONAR DE LA SECCION TODO
        if (indexPath.section == 0) {
            performSegue(withIdentifier: "toDoneItemSegue", sender: self)
        }
    }
    
    

}

