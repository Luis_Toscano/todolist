//
//  AddItemViewController.swift
//  TodoApp
//
//  Created by LuisT on 9/5/18.
//  Copyright © 2018 LuisT. All rights reserved.
//

import UIKit

class AddItemViewController: UIViewController {

    @IBOutlet weak var titleText: UITextField!
    @IBOutlet weak var locationText: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    // Para hacer opcional debe ser var. 
    var itemManager:ItemManager!;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let blackColor = UIColor.black;
        
        descriptionTextView.layer.borderWidth = 0.5;
        descriptionTextView.layer.cornerRadius = 5.0;
        descriptionTextView.layer.borderColor = blackColor.cgColor;
        titleText.layer.borderColor = blackColor.cgColor;
        titleText.layer.borderWidth = 0.5;
        titleText.layer.cornerRadius = 5.0;
        locationText.layer.borderColor = blackColor.cgColor;
        locationText.layer.borderWidth = 0.5;
        locationText.layer.cornerRadius = 5.0;
        // Do any additional setup after loading the view.
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true);
    }
    
    // MOSTRAR UN MODAL SI UNO DE LOS CAMPOS NO SE LLENEN..
    @IBAction func saveButtonPressed(_ sender: Any) {
        // USAMOS DOBLE INTERROGACION PARA ROMPER LA OPCIONALIDAD.
        let itemTitle = titleText.text ?? ""
        let itemLocation = locationText.text ?? ""
        let itemDescription = descriptionTextView.text ?? ""
        
        if (itemTitle.isEmpty || itemLocation.isEmpty || itemDescription.isEmpty) {
            // Create the alert controller
            // preferredStyle puede ser: .alert o .actionSheet
            let alertController = UIAlertController(title: "Error", message: "Debe llenar todos los campos para crear un ToDo", preferredStyle: .alert)
            
            // Create the actions
            // UIAlertAction: title, style. handler (Closure).
            // La sintaxis del closure difiere de la de JS porque el closure va despues del parentesis y no dentro.
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                UIAlertAction in
                NSLog("OK Pressed")
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
        } else {
            let item = Item(title: itemTitle, location: itemLocation, description: itemDescription);
            itemManager?.toDoItems += [item]
            // Create the alert controller
            // preferredStyle puede ser: .alert o .actionSheet
            let alertController = UIAlertController(title: "Info", message: "Se agregó un nuevo ToDo", preferredStyle: .alert)
            
            // Create the actions
            // UIAlertAction: title, style. handler (Closure).
            // La sintaxis del closure difiere de la de JS porque el closure va despues del parentesis y no dentro.
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                UIAlertAction in
                NSLog("OK Pressed")
            }
            
            // Add the actions
            alertController.addAction(okAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            
            titleText.text = ""
            locationText.text = ""
            descriptionTextView.text = ""
        }
        
    }
    
}
