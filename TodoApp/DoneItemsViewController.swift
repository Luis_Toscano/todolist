//
//  DoneItemsViewController.swift
//  TodoApp
//
//  Created by LuisT on 15/5/18.
//  Copyright © 2018 LuisT. All rights reserved.
//

import UIKit

class DoneItemsViewController: UIViewController {

    
    @IBOutlet weak var titleDetailLable: UILabel!
    @IBOutlet weak var locationDetailLabel: UILabel!
    @IBOutlet weak var descriptionDetailLabel: UILabel!
    
    // Se pasa de esta manera para solo pasar las referencias y reducir el uso de memoria.
    var itemInfo:(itemManager: ItemManager, index: Int)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleDetailLable.text =
            itemInfo?.itemManager.toDoItems[(itemInfo?.index)!].title;
        locationDetailLabel.text =
            itemInfo?.itemManager.toDoItems[(itemInfo?.index)!].location;
        descriptionDetailLabel.text =
            itemInfo?.itemManager.toDoItems[(itemInfo?.index)!].description;
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func doneButtonPressed(_ sender: Any) {
        // remove: remueve y retorna el elemento eliminado.
        let doneItem = itemInfo?.itemManager.toDoItems.remove(at: (itemInfo?.index)!);
        itemInfo?.itemManager.doneItems.append(doneItem!);
        
        // Create the alert controller
        // preferredStyle puede ser: .alert o .actionSheet
        let alertController = UIAlertController(title: "Info", message: "Todo Completed!", preferredStyle: .alert)
        
        // Create the actions
        // UIAlertAction: title, style. handler (Closure).
        // La sintaxis del closure difiere de la de JS porque el closure va despues del parentesis y no dentro.
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            
            // El closure tiene parametros, y son separados por la palabra in
            // En este caso el único parametro que recibe es: UIAlertAction
            // Como el closure se ejecuta en otro scope, es necesario hacer referencia al padre del closure (DoneItemsViewController) a través del self.
            UIAlertAction in
            self.navigationController?.popViewController(animated: true);
            
        }
        
        // Add the actions
        alertController.addAction(okAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
    }
    
  
    @IBAction func cancelButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true);
    }
    
}
